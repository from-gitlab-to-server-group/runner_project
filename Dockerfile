#Dockerfile
FROM python:3.10-slim-buster as base

WORKDIR /app

ADD . /app

RUN pip install Flask

##

FROM python:3.10-alpine3.20

WORKDIR /app

COPY --from=base /usr/local/lib/python3.10/site-packages/ /usr/local/lib/python3.10/site-packages/

COPY --from=base /app .

CMD ["python3", "app.py"]