# Flask Web Application
from flask import Flask

import os

app=Flask(__name__)

@app.route("/")
def lw():
    return "<b>Welcome to the new Web App</b>"

app.run(host='0.0.0.0', port=8080)